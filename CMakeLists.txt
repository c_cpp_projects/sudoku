cmake_minimum_required(VERSION 3.16)
project(Sudoku)

set(CMAKE_C_STANDARD 11)
set(CMAKE_C_STANDARD_REQUIRED ON)

set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

set(EXECUTABLE_NAME sudoku)

find_package(SDL2 REQUIRED)
include_directories(${SDL2_INCLUDE_DIRS})

if("Windows" STREQUAL ${CMAKE_SYSTEM_NAME})
    link_directories(${SDL2_LIBDIR})

    # Copy all .dll files
    add_custom_target("deploy")

    add_custom_command(
        TARGET "deploy"
        POST_BUILD
        COMMAND cmd /c ${CMAKE_HOME_DIRECTORY}/win64_copy_dll.bat ${CMAKE_HOME_DIRECTORY}/bin ${SDL2_BINDIR} 
    )

endif()

# Add subdirs
add_subdirectory(src)
add_subdirectory(src/log)
add_subdirectory(src/backend)
add_subdirectory(src/frontend)
add_subdirectory(src/frontend/rendering)

if("Windows" STREQUAL ${CMAKE_SYSTEM_NAME})
    add_dependencies(${EXECUTABLE_NAME} "deploy")
    target_link_libraries(${EXECUTABLE_NAME}
        mingw32 SDL2main SDL2 SDL2_image SDL2_ttf
    )
else()
    target_link_libraries(${EXECUTABLE_NAME}
        SDL2main SDL2 SDL2_image SDL2_ttf
    )
endif()