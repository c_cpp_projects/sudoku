#! /usr/bin/bash

function installPackage() {
  requiredPackage=$1

  echo "Checking for $requiredPackage..."
  isPackageOk=$(dpkg-query -W --showformat='${Status}\n' $requiredPackage | grep "install ok installed")

  if ([ -n "$isPackageOk" ]); then
    echo $isPackageOk
  else
    echo "No $requiredPackage. Setting up $requiredPackage."
    sudo apt-get --yes install $requiredPackage
  fi
}

function runAsUser() {
  runuser -l $SUDO_USER -c "$1"
}

# Check if this script is run as root.
if ([ 0 -ne $EUID ]); then
  echo "This script must be run as root."
  exit 1
fi

# Download the necessary libraries.
installPackage "libsdl2-dev"
installPackage "libsdl2-image-dev"
installPackage "libsdl2-ttf-dev"
echo

# Get the project directory.
projectDirectory=$(pwd)
buildDirectory="$projectDirectory/build"

# Destroy old CMake output if necessary, then
# create dirs again.
if ([ -d build ]); then
  runAsUser "rm -rf $buildDirectory"
fi
runAsUser "mkdir $buildDirectory"

if ([ -d bin ]); then
  runAsUser "rm -rf $projectDirectory/bin"
fi
runAsUser "mkdir $projectDirectory/bin"

if ([ -d lib ]); then
  runAsUser "rm -rf $projectDirectory/lib"
fi
runAsUser "mkdir $projectDirectory/lib"

# Execute cmake in build directory.
runAsUser "cd $buildDirectory && cmake .. -DCMAKE_BUILD_TYPE=Debug -DCMAKE_INSTALL_PREFIX=.. -DBUILD_SHARED_LIBS=OFF"

# Execute make in build directory.
runAsUser "cd $buildDirectory && make && make install"

exit 0
