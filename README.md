# Linux
## Setup
* Execute the **linux_setup.sh** as root and all necessary dependencies, as
  well as the project structure are setup.

## Build and Run
* Execute **linux_run.sh**, this script makes and make installs the
  projects modules and executes them afterwards.

# Windows
## Requirements
* cmake Version 3.16 or above
* make
* gcc
* g++

**All of those packages have to be available in the cmd as commands.**

In order to install the above listed packages, you have to have
[MinGW](https://sourceforge.net/projects/mingw-w64/) and
[chocolatey](https://chocolatey.org/install) installed.

### Installing MinGW
[Guide](https://www.osradar.com/how-to-install-mingw-on-windows-10/)

After 

### Installing cmake 3.16 or above
[Guide](https://chocolatey.org/packages/cmake)
* choco install cmake --pre 

### Installing make
[Guide](https://chocolatey.org/packages/make)
* choco install make

### Installing gcc
* mingw-get install gcc

### Installing g++
* mingw-get install g++

## Setup
1) You have to download each of the listed libraries down below.
   When choosing which file to install, take the ***Development Library***
   for ***Windows MinGW 32/64-bit***, this should be a **tar.gz** file.
   *  [SDL2](https://www.libsdl.org/download-2.0.php)
   *  [SDL2_image](https://www.libsdl.org/tmp/SDL_image/)
   *  [SDL2_ttf](https://www.libsdl.org/projects/SDL_ttf/)

2) After you have downloaded the libraries extract them in the cmd with
```bat
tar -xf library.tar.gz
```

3) Now you should have **3** folders called like the the compressed library.tar.gz files.
   It is recommanded to create a directory called **C:\Development** and put
   the library folders into it.

4) Now we have to create a config file for cmake. Create a file with the path
   **C:\Development\cmake\SDL2\sdl2-config.cmake**, you most likely have to
   create the parent directories of this file.

5) Copy paste the lines below into the **sdl2-config.cmake** file and replace
   the value of the ***prefix*** variable if the path to your **Development** folder
   is something other then **C:\Development**. **Caution replace all backslashes**
   **of the path to your Development folder with normal slashes**, so that
   **C:\Development** becomes **C:/Development**.
```cmake
set(prefix "C:/Development")
set(architecture "i686-w64-mingw32")

set(SDL2_PREFIX "${prefix}/SDL2-2.0.14/${architecture}")
set(SDL2_IMAGE_PREFIX "${prefix}/SDL2_image-2.0.5/${architecture}")
set(SDL2_TTF_PREFIX "${prefix}/SDL2_ttf-2.0.15/${architecture}")

set(SDL2_LIBDIR "${SDL2_PREFIX}/lib;${SDL2_IMAGE_PREFIX}/lib;${SDL2_TTF_PREFIX}/lib")
set(SDL2_BINDIR "${SDL2_PREFIX}/bin;${SDL2_IMAGE_PREFIX}/bin;${SDL2_TTF_PREFIX}/bin")
set(SDL2_INCLUDE_DIRS "${SDL2_PREFIX}/include/SDL2;${SDL2_IMAGE_PREFIX}/include/SDL2;${SDL2_TTF_PREFIX}/include/SDL2")
set(SDL2_LIBRARIES "-L${SDL2_LIBDIR}  -mwindows -lmingw32 -lSDL2main -lSDL2 -lSDL2_image -lSDL2_ttf")
string(STRIP "${SDL2_LIBRARIES}" SDL2_LIBRARIES)
```

6) Open a terminal in the project folder of Sudoku and execute **win64_setup.bat C:\Development\cmake\SDL2**.
   As you have probably guessed, the given argument has to be the path to the directory containing
   the **sdl2-config.cmake** file.

## Build and Run
* Execute **win64_run.bat**, this script makes and make installs the
  projects modules and executes them afterwards.