@echo off
setlocal

:: The directory in which the sdl2-config.cmake file is placed.
:: For example: C:\Development\cmake\SDL2
set "sdl2Dir=%1"

:: If no path to a sdl2 directory is given, do not execute cmake.
if "" == "%sdl2Dir%" (
    goto NO_SDL2_DIR
)
if not exist "%sdl2Dir%\" (
    goto NOT_EXISTS_SDL2_DIR
)

::  Get the project directory.
set "projectDirectory=%CD%"
set "buildDirectory=%projectDirectory%\build"

:: Destroy old CMake output if necessary, then
:: create dirs again.
if exist "%buildDirectory%" (
    rmdir /q/s "%buildDirectory%"   
)
mkdir "%buildDirectory%"   

if exist "%projectDirectory%\bin" (
    rmdir /q/s "%projectDirectory%\bin"   
)
mkdir "%projectDirectory%\bin"   

if exist "%projectDirectory%\lib" (
    rmdir /q/s "%projectDirectory%\lib"   
)
mkdir "%projectDirectory%\lib"   

:: Execute cmake in build directory.
cd %buildDirectory% && cmake .. -G "MinGW Makefiles" -DCMAKE_MAKE_PROGRAM=make -DCMAKE_C_COMPILER=gcc -DCMAKE_CXX_COMPILER=g++ -DCMAKE_BUILD_TYPE=Debug -DCMAKE_INSTALL_PREFIX=.. -DBUILD_SHARED_LIBS=OFF -DSDL2_DIR="%sdl2Dir%"

:: Execute make in build directory.
cd %buildDirectory% && make && make install

cd %projectDirectory%

goto EOF

:NO_SDL2_DIR
echo Error!
echo The directory in which the sdl2-config.cmake is placed must be given as an argument.
goto EOF

:NOT_EXISTS_SDL2_DIR
echo Error!
echo The directory in which the sdl2-config.cmake should be placed does not exist.
echo SDL2_DIR: %sdl2Dir%
goto EOF

echo.

:EOF
cmd \k