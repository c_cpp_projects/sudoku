#! /usr/bin/bash

cd build

make && make install

cd ../bin

if ([ -x ./sudoku ]); then
  ./sudoku
else
  echo sudoku is not executable!
fi
