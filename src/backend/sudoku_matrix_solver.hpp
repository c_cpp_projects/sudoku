#ifndef SOLVER_HPP
#define SOLVER_HPP

#include <stdlib.h>

namespace solverEvent {
typedef void (*Update)(int* array, size_t size);
};

class Solver {
 public:
  /**
   * @brief Create a Sudoku Solver.
   *
   * @param sudokuMatrix The SudokuMatrix that should be solved.
   * @param onUpdate
   *
   */
  Solver(unsigned int* sudokuMatrix, solverEvent::Update onUpdate = nullptr);
};

#endif  // SOLVER_HPP