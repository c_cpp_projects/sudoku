#ifndef CREATOR_HPP
#define CREATOR_HPP

#include <stdlib.h>

#include <algorithm>
#include <vector>

#include "../log/log.hpp"

class SudokuMatrixGenerator {
 private:
  std::vector<int>* matrix;
  int _cellSize;
  int matrixSize;

 public:
  /**
   * @brief Create a Sudoku Matrix Generator.
   *
   * @param matrixSize The Size of The Sudokumatrix (For Example: 3x3)
   *
   */
  SudokuMatrixGenerator(int matrixSize);

  /**
   * @brief Generates a Sudoku Matrix and saves it in the private Matrix vector.
   *
   */
  void generateMatrix();

  /**
   * @brief Returns the generated Matrix(Vector).
   *
   */
  std::vector<int>* getMatrix();

  /**
   * @brief Returns a solvable Sudoku Game (Matrix) based on an (already generated)
   * solution.
   *
   * @param leftOutCells determents the Cells that will be left out to help the solver
   * Solv the Sodoku.
   *
   */
  std::vector<int>* getMatrixToBeSolved(int leftOutCells);

  /**
   * @brief Returns True if there is no other same value in horizontal or vertical line to
   * the target
   *
   * @param leftOutCells determents the Cells that will be left out to help the solver
   * Solv the Sodoku.
   *
   */
  bool isAlone(std::vector<int>* matrix, int targetIdx, int targetValue);
};

#endif