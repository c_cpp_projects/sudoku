#include "sudoku_matrix_generator.hpp"

SudokuMatrixGenerator::SudokuMatrixGenerator(int cellSize) : _cellSize(cellSize) {
  matrixSize = _cellSize * _cellSize;
  Log::d("SudokuMatrixGenerator", "In SudokuMatrixGenerator constructor");
}

std::vector<int>* SudokuMatrixGenerator::getMatrix() { return matrix; }

bool SudokuMatrixGenerator::isAlone(std::vector<int>* matrix, int targetIdx,
                                    int targetValue) {
  // Looks 4 horizantal partners
  for (int i = matrixSize * (targetIdx / matrixSize);
       i < matrixSize * ((targetIdx / matrixSize) + 1); i++) {
    // ignore the targetIdx
    if (i == targetIdx) {
    } else {
      // returns false if there is one
      if (matrix->at(i) == targetValue) return false;
    }
  }
  for (int i = targetIdx % matrixSize; i / matrixSize < matrixSize; i = i + matrixSize) {
    // ignore the targetIdx
    if (i == targetIdx) {
    } else {
      // returns false if there is one
      if (matrix->at(i) == targetValue) return false;
    }
  }
  return true;
}

void SudokuMatrixGenerator::generateMatrix() {
  // Create an empty vector
  std::vector<int> vect;
  // Fill the Vector with 0(=empty Cells)

  for (int i = 0; i < matrixSize * matrixSize; i++) vect.push_back(0);
  for (int k = 1; k < matrixSize + 1; k++) {
    for (int i = 0; i < _cellSize; i++) {
      for (int j = 0; j < _cellSize; j++) {
        // Cell based Idx
        int randomIdx = std::rand() % 9;
        // Matrix based Idx (translated in Matrix World)
        int realIdx =
            (randomIdx / _cellSize) * matrixSize + randomIdx % _cellSize + i * matrixSize;

        std::vector<bool> allPossibilityChache;

        for (int l = 0; l < matrixSize; l++) vect.push_back(false);

        allPossibilityChache[randomIdx] = true;

        while (!(isAlone(matrix, realIdx, k) || matrix->at(realIdx) == 0)) {
          randomIdx = std::rand() % 9;

          allPossibilityChache[randomIdx] = true;

          realIdx = (randomIdx / _cellSize) * matrixSize + randomIdx % _cellSize +
                    i * matrixSize;

          const std::vector<bool>::iterator iter =
              find_if(allPossibilityChache.begin(), allPossibilityChache.end(),
                      [](bool b) -> bool { return !b; });

          if (*iter) {
            break;
          }
        }
        if (isAlone(matrix, realIdx, k) || matrix->at(realIdx) == 0) {
          matrix->at(realIdx) = k;
        }
      }
    }
  }
}