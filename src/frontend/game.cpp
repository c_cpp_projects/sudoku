#include "game.hpp"

Game* Game::instance = nullptr;

const string Game::TAG = "Game";

Game* Game::getInstance() { return instance; }

Game* Game::initInstance(string title, int width, int height, unsigned int fps) {
  if (nullptr == instance) instance = new Game(title, width, height, fps);
  return instance;
}

void Game::disposeInstance() {
  if (nullptr != instance) delete instance;
}

/******************** Definition of Public Functions ********************/

Game::Game(string title, int width, int height, unsigned int fps)
    : fpsCapper(fps),
      _title(title),
      _width(width),
      _height(height),
      _isPaused(false),
      _isStopped(true),
      _window(nullptr),
      _renderer(nullptr),
      _font(nullptr) {
  if (initSDL()) {
    _onEvent = StreamController<gameStructs::OnEvent>();
    _onPaused = StreamController<gameStructs::OnPaused>();
    _onStopped = StreamController<gameStructs::OnStopped>();
    _onRender = StreamController<gameStructs::OnRender>();

    if (!loadMedia()) {
      Log::w(TAG.c_str(), "Warning! Could not load all media for Game.");
    }
  } else {
    dispose();
  }
}

Game::~Game() { dispose(); }

void Game::start() {
  if (_isStopped) {
    startGameLoop();
  } else if (_isPaused) {
    Log::i(TAG.c_str(), "resumed");
    _isPaused = false;
  }
}

void Game::pause() {
  Log::i(TAG.c_str(), "paused");
  _isStopped = false;
  _isPaused = true;
  _onPaused.add({*this});
}

void Game::stop() {
  _isStopped = true;
  _isPaused = false;
  _onStopped.add({*this});
}

Stream<gameStructs::OnEvent>* Game::onEvent() { return _onEvent.getStream(); }

Stream<gameStructs::OnPaused>* Game::onPaused() { return _onPaused.getStream(); }

Stream<gameStructs::OnStopped>* Game::onStopped() { return _onStopped.getStream(); }

Stream<gameStructs::OnRender>* Game::onRender() { return _onRender.getStream(); }

bool Game::isPlaying() const { return !_isPaused && !_isStopped; }

bool Game::isPaused() const { return _isPaused; }

bool Game::isStopped() const { return _isStopped; }

TTF_Font* Game::getFont() const { return _font; }

SDL_Renderer* Game::getRenderer() const { return _renderer; }

bool Game::setFont(string font, int size) {
  if (nullptr != _font) TTF_CloseFont(_font);

  // Open the fontPath called _fontPath
  font = "../assets/fonts/" + font;
  _font = TTF_OpenFont(font.c_str(), size);

  if (nullptr == _font) {
    Log::e(TAG.c_str(), "Could not open font %s!", font.c_str());
    Log::e("TTF_GetError", "%s", TTF_GetError());
    return false;
  } else {
    Log::i(TAG.c_str(), "Successfully opened font %s", font.c_str());
  }

  return true;
}

bool Game::setFont(TTF_Font* font) {
  if (nullptr != _font) TTF_CloseFont(_font);
  _font = font;
  if (nullptr == _font) return false;
  return true;
}

/******************** Definition of Private Functions ********************/

bool Game::initSDL() {
  if (0 != SDL_Init(SDL_INIT_VIDEO)) {
    Log::e(TAG.c_str(), "SDL could not initialize! SDL_Error: %s\n", SDL_GetError());
    return false;
  }
  Log::i(TAG.c_str(), "Successfully initialized SDL!");

  // Set texture filtering to linear
  if (!SDL_SetHint(SDL_HINT_RENDER_SCALE_QUALITY, "1")) {
    Log::w(TAG.c_str(), "Warning: Linear texture filtering not enabled!");
  } else {
    Log::i(TAG.c_str(), "Successfully enabled linear texture filtering!");
  }

  // Create window
  _window = SDL_CreateWindow(_title.c_str(), SDL_WINDOWPOS_UNDEFINED,
                             SDL_WINDOWPOS_UNDEFINED, _width, _height, SDL_WINDOW_SHOWN);
  if (nullptr == _window) {
    Log::e(TAG.c_str(), "Window could not be created! SDL_Error: %s\n", SDL_GetError());
    return false;
  }
  Log::i(TAG.c_str(), "Successfully created a SDL_Window!");

  _renderer = SDL_CreateRenderer(_window, -1, SDL_RENDERER_ACCELERATED);
  if (nullptr == _renderer) {
    Log::e(TAG.c_str(), "Renderer could not be created! SDL Error: %s\n", SDL_GetError());
    return false;
  }
  Log::i(TAG.c_str(), "Successfully created a SDL_Renderer!");

  // initialize SDL_image
  if (0 == IMG_Init(IMG_INIT_PNG)) {
    Log::e(TAG.c_str(), "Could not initialize SDL_image!\n SDL_image Error: %s\n\n",
           IMG_GetError());
    return false;
  }
  Log::i(TAG.c_str(), "Successfully initialized SDL_image subsystem!");

  // initialize SDL_ttf
  if (0 != TTF_Init()) {
    Log::e(TAG.c_str(), "Could not initialize SDL_ttf!\n SDL_ttf Error: %s\n\n",
           TTF_GetError());
    return false;
  }

  Log::i(TAG.c_str(), "Successfully initialized SDL_ttf subsystem!");

  return true;
}

bool Game::loadMedia() { return true; }

void Game::dispose() {
  Log::d(TAG.c_str(), "dispose");

  if (nullptr != _font) {
    Log::i(TAG.c_str(), "Closed font");
    TTF_CloseFont(_font);
    _font = nullptr;
  }

  SDL_DestroyRenderer(_renderer);
  _renderer = nullptr;
  Log::i(TAG.c_str(), "Destroyed SDL_Renderer");

  SDL_DestroyWindow(_window);
  _window = nullptr;
  Log::i(TAG.c_str(), "Destroyed SDL_Window");

  TTF_Quit();
  Log::i(TAG.c_str(), "Quit SDL_ttf subsystem");
  IMG_Quit();
  Log::i(TAG.c_str(), "Quit SDL_image subsystem");
  SDL_Quit();
  Log::i(TAG.c_str(), "Quit SDL subsystem");
}

void Game::startGameLoop() {
  _isPaused = false;
  _isStopped = false;

  fpsCapper.begin();

  while (!_isStopped) {
    fpsCapper.start();

    if (0 != SDL_PollEvent(&_event)) _onEvent.add({*this, _renderer, _event});

    if (isPlaying()) {
      SDL_SetRenderDrawColor(_renderer, 255, 255, 255, 255);
      SDL_RenderClear(_renderer);
      _onRender.add({*this, _renderer});
      SDL_RenderPresent(_renderer);
    }

    Log::v(TAG.c_str(), "Average FPS: %d", (int) fpsCapper.getAverageFPS());

    fpsCapper.cap();
  }

  Log::i(TAG.c_str(), "Stopped Game loop");
}