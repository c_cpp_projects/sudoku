#include "fps_capper.hpp"

FPSCapper::FPSCapper(unsigned int fps)
    : frameCount(0), averageFps(0), frameTicks(0), _fps(fps) {
  ticksPerFrame = 1000 / fps;
}

FPSCapper::~FPSCapper() {
  if (capTimer.isRunning()) capTimer.stop();
  if (fpsTimer.isRunning()) fpsTimer.stop();
}

void FPSCapper::begin() {
  frameCount = 0;
  fpsTimer.start();
  averageFps = 0;
}

void FPSCapper::start() {
  capTimer.start();
  // Calculate and correct fps
  averageFps = frameCount / (fpsTimer.getTicks() / 1000.f);
  if (2000000 < averageFps) averageFps = 0;
}

void FPSCapper::cap() {
  frameCount++;
  // wait if frame finished early
  frameTicks = capTimer.getTicks();
  if (ticksPerFrame > frameTicks) SDL_Delay(ticksPerFrame - frameTicks);
}

float FPSCapper::getAverageFPS() const { return averageFps; }
