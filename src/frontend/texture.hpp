#ifndef TEXTURE_HPP
#define TEXTURE_HPP

#include <SDL.h>
#include <SDL_image.h>
#include <SDL_ttf.h>

#include <string>

#include "../log/log.hpp"

class Texture {
 private:
  // The actual hardware texture
  SDL_Texture* _texture;
  std::string _textureText;

  // dimensions
  int _width;
  int _height;

 public:
  static const string TAG;

  // Initializes variables
  Texture();

  // Deallocates memory
  virtual ~Texture();

  // Gets this textures width
  int getWidth() const;

  // Gets this textures height
  int getHeight() const;

  std::string getText() const;

  bool isLoaded() const;

  // Set color modulation
  void setColor(const Uint8 red, const Uint8 green, const Uint8 blue) const;

  // Set blending
  void setBlendMode(const SDL_BlendMode blending) const;

  // Set alpha modulation
  void setAlpha(const Uint8 alpha) const;

  // Loads image at specified path
  bool loadFromFile(SDL_Renderer* renderer, const std::string path);

#if defined(SDL_TTF_MAJOR_VERSION)
  // Creates image from font string
  bool loadFromRenderedText(SDL_Renderer* renderer, TTF_Font* font,
                            const std::string textureText, const SDL_Color textColor);
#endif

  // Deallocates texture
  void dispose();

  // Renders texture at given point
  void render(SDL_Renderer* renderer, const int x, const int y,
              const SDL_Rect* clip = nullptr, const double angle = 0.0,
              const SDL_Point* center = nullptr,
              const SDL_RendererFlip flip = SDL_FLIP_NONE) const;
};

#endif  // TEXTURE_HPP