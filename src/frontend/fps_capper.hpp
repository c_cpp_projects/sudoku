#ifndef FPS_CAPPER_HPP
#define FPS_CAPPER_HPP

#include <SDL.h>

#include "timer.hpp"

class FPSCapper {
 private:
  Uint32 frameCount;
  float averageFps;
  unsigned int frameTicks;
  unsigned int ticksPerFrame;

  Timer fpsTimer;
  Timer capTimer;

  unsigned int _fps;

 public:
  FPSCapper(unsigned int fps = 60);
  virtual ~FPSCapper();

  void begin();

  void start();

  void cap();

  float getAverageFPS() const;
};

#endif  // FPS_CAPPER_HPP