#include "render_matrix.hpp"

/******************** Definition of Public Functions ********************/

RenderMatrix::RenderMatrix(int size, size_t matrixSize, const char* elements,
                           const char* font)
    : RenderObject(size, size), _matrixSize(matrixSize) {
  _font = RenderTile::openPerfectFont(font, getTileSize());

  const size_t elementsLength = _matrixSize * _matrixSize;
  tiles.resize(elementsLength);

  for (size_t i = 0; i < elementsLength; i++)
    tiles[i] = new RenderTile(getTileSize(), elements[i], _font);
}

RenderMatrix::~RenderMatrix() {
  if (nullptr != _font) TTF_CloseFont(_font);
  for (size_t i = 0; i < _matrixSize * _matrixSize; i++) delete tiles[i];
}

int RenderMatrix::getSize() const { return getHeight(); }

void RenderMatrix::render(SDL_Renderer* renderer, int x, int y) {
  for (size_t row = 0; row < _matrixSize; row++)
    for (size_t column = 0; column < _matrixSize; column++)
      tiles[column + row * _matrixSize]->render(renderer, column * getTileSize(),
                                                row * getTileSize());
}

/******************** Definition of Private Functions ********************/

int RenderMatrix::getTileSize() const { return getSize() / _matrixSize; }