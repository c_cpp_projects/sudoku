#include "render_object.hpp"

/******************** Definition of Public Functions ********************/

RenderObject::RenderObject(int width, int height, SDL_Color color)
    : _width(width), _height(height), _color(color), texture(nullptr) {}

RenderObject::~RenderObject() {
  if (nullptr != texture) {
    delete texture;
    texture = nullptr;
  }
}

int RenderObject::getHeight() const { return _height; }

int RenderObject::getWidth() const { return _width; }

/******************** Definition of Protected Functions ********************/

SDL_Rect RenderObject::getClip() const { return SDL_Rect({0, 0, _width, _height}); }

void RenderObject::setRendererDrawColor(SDL_Renderer* renderer) const {
  SDL_SetRenderDrawColor(renderer, _color.r, _color.g, _color.b, _color.a);
}