#ifndef RENDER_MATRIX_HPP
#define RENDER_MATRIX_HPP

#include <SDL_ttf.h>

#include <vector>

#include "render_object.hpp"
#include "render_tile.hpp"

class RenderMatrix : public RenderObject {
 private:
  TTF_Font* _font;

  size_t _matrixSize;
  std::vector<RenderTile*> tiles;

  int getTileSize() const;

 public:
  /**
   * @brief Constructor for a RenderMatrix.
   *
   *
   * @param size The size of this RenderMatrix in pixels.
   *
   * @param matrixSize The size of the matrix displayed by a RenderMatrix.
   *                   (e.g. matrixSize = 3; => 3x3 Matrix).
   *
   * @param elements A char array containing all the elements
   *                 that are displayed in this RenderMatrix.
   *                 elements must contain at least matrixSize * matrixSize
   *                 characters, else an error occurs.
   *
   * @param font The font that will be used to display each element.
   */
  RenderMatrix(int size, size_t matrixSize, const char* elements, const char* font);

  virtual ~RenderMatrix();

  int getSize() const;

  void render(SDL_Renderer* renderer, int x, int y);
};

#endif  // RENDER_MATRIX_HPP