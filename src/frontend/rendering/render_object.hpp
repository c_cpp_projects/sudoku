#ifndef RENDER_OBJECT_HPP
#define RENDER_OBJECT_HPP

#include <SDL.h>

#include "../texture.hpp"

class RenderObject {
 protected:
  // The height of this field in pixels.
  int _height;

  // The width of this field in pixels.
  int _width;

  Texture* texture;

  SDL_Color _color;

  // Get the clip for the texture of this RenderObject.
  SDL_Rect getClip() const;

  void setRendererDrawColor(SDL_Renderer* renderer) const;

 public:
  RenderObject(int width, int height, SDL_Color color = {0, 0, 0, 255});

  virtual ~RenderObject();

  int getHeight() const;

  int getWidth() const;

  virtual void render(SDL_Renderer* renderer, int x, int y) = 0;
};

#endif  // RENDER_OBJECT_HPP