#ifndef RENDER_TILE_HPP
#define RENDER_TILE_HPP

#include <SDL.h>
#include <SDL_ttf.h>

#include "../../log/log.hpp"
#include "../game.hpp"
#include "../texture.hpp"
#include "render_object.hpp"

class RenderTile : public RenderObject {
 private:
  char _content;

 protected:
  TTF_Font* _font;

 public:
  static const std::string TAG;

  /**
   * @brief Open a TTF_Font with a pixelsize big
   *        enough to perfectly fit into a RenderTile
   *        with param size as its size.
   *
   * @param font Path to the font you want to use.
   *
   * @param size The size of your RenderTile.
   */
  static TTF_Font* openPerfectFont(const char* font, int size);

  RenderTile(int size, char content, TTF_Font* font = nullptr);

  virtual ~RenderTile();

  /**
   * @brief getSize(), getWidth() and getHeight() return the same value.
   *
   * @return The size of this RenderTile.
   */
  int getSize() const;

  void setContent(SDL_Renderer* renderer, char content);

  void render(SDL_Renderer* renderer, int x, int y);
};

#endif  // RENDER_TILE_HPP