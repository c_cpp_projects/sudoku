#include "render_tile.hpp"

const std::string RenderTile::TAG = "RenderTile";

TTF_Font* RenderTile::openPerfectFont(const char* font, int size) {
  return TTF_OpenFont(font, size);
}

/******************** Definition of Public Functions ********************/

RenderTile::RenderTile(int size, char content, TTF_Font* font)
    : RenderObject(size, size), _content(content), _font(font) {
  texture = new Texture();
}

RenderTile::~RenderTile() {}

int RenderTile::getSize() const { return getHeight(); }

void RenderTile::setContent(SDL_Renderer* renderer, char content) {
  _content = content;
  if (nullptr != _font) {
    if (!texture->loadFromRenderedText(renderer, _font, string(1, _content), _color)) {
      Log::w(TAG.c_str(),
             "WARNING! Could not load texture for RenderTile with content %c.", _content);
      texture->dispose();
    }
  }
}

void RenderTile::render(SDL_Renderer* renderer, int x, int y) {
  setRendererDrawColor(renderer);

  SDL_Rect tile = getClip();
  tile.x = x;
  tile.y = y;
  SDL_RenderDrawRect(renderer, &tile);

  if (!texture->isLoaded() && nullptr != _font) {
    if (!texture->loadFromRenderedText(renderer, _font, string(1, _content), _color)) {
      Log::w(TAG.c_str(),
             "WARNING! Could not load texture for RenderTile with content %c.", _content);
      texture->dispose();
    }
  } else {
    texture->render(renderer, x + ((int) ceilf((float) texture->getWidth() * 0.5f)),
                    y + ((int) ceilf(0.14f * (texture->getHeight()) - 0.5f * _height)));
  }
}
