#include "texture.hpp"

const string Texture::TAG = "Texture";

/******************** Definition of Public Functions ********************/

Texture::Texture() : _texture(nullptr), _width(0), _height(0) {}

Texture::~Texture() {
  // Deallocate
  dispose();
}

bool Texture::loadFromFile(SDL_Renderer* renderer, const std::string path) {
  // Get rid of preexisting _texture
  dispose();

  // Load image at specified path
  SDL_Surface* loadedSurface = IMG_Load(path.c_str());
  if (nullptr == loadedSurface) {
    Log::e(TAG.c_str(), "Unable to load image %s!", path.c_str());
    Log::e("IMG_GetError", "%s", IMG_GetError());
    return false;
  }

  // Color key image
  SDL_SetColorKey(loadedSurface, SDL_TRUE,
                  SDL_MapRGB(loadedSurface->format, 0, 0xFF, 0xFF));

  _texture = SDL_CreateTextureFromSurface(renderer, loadedSurface);
  if (nullptr == _texture) {
    Log::e(TAG.c_str(), "Unable to create _texture from %s!", path.c_str());
    Log::e("SDL_GetError", "%s", SDL_GetError());
    return false;
  }

  // Get image dimensions
  _width = loadedSurface->w;
  _height = loadedSurface->h;

  // Get rid of old loaded surface
  SDL_FreeSurface(loadedSurface);

  return true;
}

#if defined(SDL_TTF_MAJOR_VERSION)
bool Texture::loadFromRenderedText(SDL_Renderer* renderer, TTF_Font* font,
                                   const std::string textureText,
                                   const SDL_Color textColor) {
  // Get rid of preexisting _texture
  dispose();

  _textureText = textureText;

  // Render text surface
  SDL_Surface* textSurface = TTF_RenderText_Solid(font, textureText.c_str(), textColor);
  if (nullptr == textSurface) {
    Log::e(TAG.c_str(), "Unable to render text surface!");
    Log::e("TTF_GetError", "%s", TTF_GetError());
    return false;
  }

  // Create _texture from surface pixels
  _texture = SDL_CreateTextureFromSurface(renderer, textSurface);
  if (nullptr == _texture) {
    Log::e(TAG.c_str(), "Unable to create _texture from rendered text!");
    Log::e("SDL_GetError", "%s", SDL_GetError());
    return false;
  }

  // Get image dimension
  _width = textSurface->w;
  _height = textSurface->h;

  // Get rid of old surface
  SDL_FreeSurface(textSurface);

  Log::i(TAG.c_str(), "Successfully created texture from text: %s", _textureText.c_str());

  return true;
}
#endif

void Texture::dispose() {
  // Free _texture if it exists
  if (nullptr != _texture) {
    SDL_DestroyTexture(_texture);
    _texture = nullptr;
    _width = 0;
    _height = 0;
  }
}

void Texture::setColor(const Uint8 red, const Uint8 green, const Uint8 blue) const {
  // Modulate _texture
  SDL_SetTextureColorMod(_texture, red, green, blue);
}

void Texture::setBlendMode(const SDL_BlendMode blending) const {
  SDL_SetTextureBlendMode(_texture, blending);
}

void Texture::setAlpha(const Uint8 alpha) const {
  SDL_SetTextureAlphaMod(_texture, alpha);
}

void Texture::render(SDL_Renderer* renderer, const int x, const int y,
                     const SDL_Rect* clip, const double angle, const SDL_Point* center,
                     const SDL_RendererFlip flip) const {
  // Set rendering space and render to screen
  SDL_Rect renderQuad = {x, y, _width, _height};

  // Set clip rendering dimensions
  if (nullptr != clip) {
    renderQuad.w = clip->w;
    renderQuad.h = clip->h;
  }

  SDL_RenderCopyEx(renderer, _texture, clip, &renderQuad, angle, center, flip);
}

int Texture::getWidth() const { return _width; }

int Texture::getHeight() const { return _height; }

std::string Texture::getText() const { return _textureText; }

bool Texture::isLoaded() const { return nullptr != _texture; }
