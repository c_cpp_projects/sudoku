#ifndef TIMER_HPP
#define TIMER_HPP

#include <SDL.h>

class Timer {
 private:
  // The clock time when the timer started
  Uint32 startTicks;

  // THe ticks stored when the timer was paused
  Uint32 pausedTicks;

  // The timer status
  bool started;
  bool paused;

 public:
  // Initialized variables
  Timer();

  // The various clock actions
  void start();
  void stop();
  void pause();
  void resume();

  // Gets the timers time
  Uint32 getTicks() const;

  // Checks the status of the timer
  bool isRunning() const;
  bool isPaused() const;
};

#endif  // TIMER_HPP