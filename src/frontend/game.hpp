#ifndef GAME_HPP
#define GAME_HPP

#include <SDL.h>
#include <SDL_image.h>
#include <SDL_ttf.h>

#include <functional>
#include <string>

#include "../log/log.hpp"
#include "../stream/stream.tpp"
#include "../stream/stream_controller.tpp"
#include "fps_capper.hpp"

using namespace std;

class Game;

namespace gameStructs {
typedef struct {
  Game& game;
  SDL_Renderer* renderer;
  SDL_Event& event;
} OnEvent;

typedef struct {
  Game& game;
} OnPaused;

typedef OnPaused OnStopped;

typedef struct {
  Game& game;
  SDL_Renderer* renderer;
} OnRender;

}  // namespace gameStructs

class Game {
 private:
  static Game* instance;

  FPSCapper fpsCapper;

  int _width;
  int _height;
  string _title;

  bool _isPaused;
  bool _isStopped;

  SDL_Window* _window;
  SDL_Renderer* _renderer;
  SDL_Event _event;
  TTF_Font* _font;

  StreamController<gameStructs::OnEvent> _onEvent;
  StreamController<gameStructs::OnPaused> _onPaused;
  StreamController<gameStructs::OnStopped> _onStopped;
  StreamController<gameStructs::OnRender> _onRender;

  Game(string title, int width, int height, unsigned int fps);

  bool initSDL();
  bool loadMedia();
  void dispose();
  void startGameLoop();

 public:
  static const string TAG;

  static Game* getInstance();

  static Game* initInstance(string title, int width = 1080, int height = 720,
                            unsigned int fps = 60);

  static void disposeInstance();

  virtual ~Game();

  bool isPlaying() const;

  bool isPaused() const;

  bool isStopped() const;

  Stream<gameStructs::OnEvent>* onEvent();

  Stream<gameStructs::OnPaused>* onPaused();

  Stream<gameStructs::OnStopped>* onStopped();

  Stream<gameStructs::OnRender>* onRender();

  // Get the currently loaded font if there is one, else nullptr.
  TTF_Font* getFont() const;

  SDL_Renderer* getRenderer() const;

  // Load a font after the initial initialization process.
  bool setFont(string font, int size);

  // Set font to an already loaded one.
  bool setFont(TTF_Font* font);

  void start();

  void pause();

  void stop();
};

#endif  // GAME_HPP