#include "timer.hpp"

Timer::Timer() : startTicks(0), pausedTicks(0), paused(false), started(false) {}

void Timer::start() {
  // Start the timer
  started = true;

  // unpause the timer
  paused = false;

  // Get the current clock time
  startTicks = SDL_GetTicks();
  pausedTicks = 0;
}

void Timer::stop() {
  // Stop the timer
  started = false;

  // Unpause the timer
  paused = false;

  // Clear tick variables
  startTicks = 0;
  pausedTicks = 0;
}

void Timer::pause() {
  // If the timer is running and is not already paused
  if (started && !paused) {
    // pause timer
    paused = true;

    // Calculate the paused ticks
    pausedTicks = SDL_GetTicks() - startTicks;
    startTicks = 0;
  }
}

void Timer::resume() {
  // If the timer is running and paused
  if (started && paused) {
    // resume timer
    paused = false;

    // Reset the starting ticks
    startTicks = SDL_GetTicks() - pausedTicks;

    // Reset paused ticks
    pausedTicks = 0;
  }
}

Uint32 Timer::getTicks() const {
  // If the timer is running
  if (started) {
    // if the timer is paused
    if (paused) {
      // return the number of ticks when the timer was paused
      return pausedTicks;
    } else {
      // return the current time minus the start time
      return SDL_GetTicks() - startTicks;
    }
  }

  return 0;
}

bool Timer::isRunning() const { return started; }

bool Timer::isPaused() const { return paused && started; }
