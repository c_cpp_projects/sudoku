#include <SDL.h>

#include <functional>
#include <iostream>
#include <string>

#include "assets.hpp"
#include "frontend/game.hpp"
#include "frontend/rendering/render_matrix.hpp"
#include "frontend/rendering/render_tile.hpp"
#include "log/log.hpp"
#include "stream/aliases.tpp"
#include "stream/stream.tpp"
#include "stream/stream_controller.tpp"
#include "stream/stream_subscription.tpp"

void test1();
void test2();

int main(int argc, char* argv[]) {
  test1();
  // test2();

  return 0;
}

void test1() {
  Log::logLevel = Log::DEBUG;

  // int tileSize = 15;
  // int tileSize = 21;
  // int tileSize = 29;
  // int tileSize = 37;
  // int tileSize = 44;
  // int tileSize = 57;
  // int tileSize = 63;
  // int tileSize = 70;
  // int tileSize = 20;
  // int tileSize = 30;
  // int tileSize = 40;
  int tileSize = 50;

  Game& g = *Game::initInstance(APPLICATION_NAME, SCREEN_SIZE, SCREEN_SIZE, SCREEN_FPS);

  {
    g.setFont(RenderTile::openPerfectFont(FONTS("DotGothic16-Regular.ttf"), tileSize));

    SDL_Rect rect = {SCREEN_SIZE / 2, SCREEN_SIZE / 2, 20, 20};

    RenderTile tile = RenderTile(tileSize, '9', g.getFont());
    int tileX = (SCREEN_SIZE - tile.getWidth()) / 2;
    int tileY = (SCREEN_SIZE - tile.getHeight()) / 2;

    g.onEvent()->listen([&tile, &tileX, &tileY](gameStructs::OnEvent data) {
      if (SDL_QUIT == data.event.type) data.game.stop();
      if (SDL_MOUSEBUTTONDOWN == data.event.type) {
        if (data.game.isPlaying()) {
          data.game.pause();
        } else {
          data.game.start();
        }
      }
      if (SDL_KEYDOWN == data.event.type) {
        switch (data.event.key.keysym.sym) {
          case SDLK_UP:
            tileY--;
            break;

          case SDLK_RIGHT:
            tileX++;
            break;

          case SDLK_DOWN:
            tileY++;
            break;

          case SDLK_LEFT:
            tileX--;
            break;

          case SDLK_0:
            tile.setContent(data.renderer, '0');
            break;

          case SDLK_1:
            tile.setContent(data.renderer, '1');
            break;

          case SDLK_2:
            tile.setContent(data.renderer, '2');
            break;

          case SDLK_3:
            tile.setContent(data.renderer, '3');
            break;

          case SDLK_4:
            tile.setContent(data.renderer, '4');
            break;

          case SDLK_5:
            tile.setContent(data.renderer, '5');
            break;

          case SDLK_6:
            tile.setContent(data.renderer, '6');
            break;

          case SDLK_7:
            tile.setContent(data.renderer, '7');
            break;

          case SDLK_8:
            tile.setContent(data.renderer, '8');
            break;

          case SDLK_9:
            tile.setContent(data.renderer, '9');
            break;

          default:
            break;
        }
      }
    });
    g.onRender()->listen([&rect, &tile, &tileX, &tileY](gameStructs::OnRender data) {
      tile.render(data.renderer, tileX, tileY);
    });

    g.start();
  }

  Game::disposeInstance();
}

void test2() {
  Log::logLevel = Log::DEBUG;

  Game& g = *Game::initInstance(APPLICATION_NAME, SCREEN_SIZE, SCREEN_SIZE, SCREEN_FPS);

  // All of the following code has to be in a seperate scope,
  // because otherwise a crash occurs when the RenderMatrix Deconstructor
  // is called after disposing the global Game Instance, which
  // quits all SDL2 subsystems.
  {
    char elems[9] = {'1', '2', '3', '4', '5', '6', '7', '8', '9'};

    RenderMatrix matrix =
        RenderMatrix(SCREEN_SIZE / 2, 3, elems, FONTS("DotGothic16-Regular.ttf"));

    g.onEvent()->listen([](gameStructs::OnEvent data) {
      if (SDL_QUIT == data.event.type) data.game.stop();
      if (SDL_MOUSEBUTTONDOWN == data.event.type) {
        if (data.game.isPlaying()) {
          data.game.pause();
        } else {
          data.game.start();
        }
      }
    });
    g.onRender()->listen(
        [&matrix](gameStructs::OnRender data) { matrix.render(data.renderer, 0, 0); });

    g.start();
  }

  Game::disposeInstance();
}