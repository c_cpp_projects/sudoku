#ifndef ASSETS_HPP
#define ASSETS_HPP

#define ASSETS      "../assets"
#define FONTS(font) ASSETS "/fonts/" font

// #define TEXTURES(texture) (ASSETS "/textures/" texture)

#define APPLICATION_NAME "Sudoku"
#define SCREEN_SIZE      (400)
#define SCREEN_FPS       (60)

#endif  // ASSETS_HPP