#include "log.hpp"

#define PRINTLN_WITH_VA_LIST(id)           \
  ({                                       \
    va_list args;                          \
    va_start(args, format);                \
    vsprintf(message, format, args);       \
    va_end(args);                          \
    printf("%s", getTimeString().c_str()); \
    printf(" %c", id);                     \
    printf("/%s", copiedTag.c_str());      \
    printf(": %s", message);               \
    printf("\n");                          \
  })

const int Log::VERBOSE = 2;
const int Log::DEBUG = 3;
const int Log::INFO = 4;
const int Log::WARN = 5;
const int Log::ERROR = 6;
const int Log::OFF = 7;

int Log::logLevel = Log::VERBOSE;

string Log::getTimeString() {
  timespec spec;
  string millis;

#ifdef WIN32
  clock_gettime(CLOCK_REALTIME, &spec);
  string timeStr = "";
  time_t tt = spec.tv_sec;
  struct tm* values = localtime(&tt);

  timeStr += to_string(values->tm_year + 1900) + '-';
  timeStr += to_string(values->tm_mon + 1) + '-';
  timeStr += to_string(values->tm_mday) + ' ';

  timeStr += to_string(values->tm_hour);
  if (0 == values->tm_hour) timeStr += '0';
  timeStr += ':';

  timeStr += to_string(values->tm_min);
  if (0 == values->tm_min) timeStr += '0';
  timeStr += ':';

  timeStr += to_string(values->tm_sec);
  if (0 == values->tm_sec) timeStr += '0';
#else
  char timeStr[20];
  timespec_get(&spec, TIME_UTC);
  strftime(timeStr, 20, "%F %T", gmtime(&spec.tv_sec));
#endif

  millis = to_string(spec.tv_nsec / 1000000UL);
  if (1 == millis.length()) {
    millis += "00";
  } else if (2 == millis.length()) {
    millis += '0';
  }

  millis = string(timeStr) + '.' + millis;

  return millis;
}

void Log::v(const char* tag, const char* format, ...) {
  const string copiedTag(tag);
  const size_t formatLen = strlen(format);
  if (VERBOSE >= logLevel) {
    char message[formatLen];
    PRINTLN_WITH_VA_LIST('V');
  }
}

void Log::d(const char* tag, const char* format, ...) {
  const string copiedTag(tag);
  const size_t formatLen = strlen(format);
  if (DEBUG >= logLevel) {
    char message[formatLen];
    PRINTLN_WITH_VA_LIST('D');
  }
}

void Log::i(const char* tag, const char* format, ...) {
  const string copiedTag(tag);
  const size_t formatLen = strlen(format);
  if (INFO >= logLevel) {
    char message[formatLen];
    PRINTLN_WITH_VA_LIST('I');
  }
}

void Log::w(const char* tag, const char* format, ...) {
  const string copiedTag(tag);
  const size_t formatLen = strlen(format);
  if (WARN >= logLevel) {
    char message[formatLen];
    PRINTLN_WITH_VA_LIST('W');
  }
}

void Log::e(const char* tag, const char* format, ...) {
  const string copiedTag(tag);
  const size_t formatLen = strlen(format);
  if (ERROR >= logLevel) {
    char message[formatLen];
    PRINTLN_WITH_VA_LIST('E');
  }
}

void Log::println(const char* tag, const char* format, ...) {
  const string copiedTag(tag);
  const size_t formatLen = strlen(format);
  char message[formatLen];
  PRINTLN_WITH_VA_LIST('A');
}