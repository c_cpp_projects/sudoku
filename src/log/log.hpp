#ifndef LOG_HPP
#define LOG_HPP

#include <cstdarg>
#include <cstring>
#include <ctime>
#include <iostream>
#include <string>

using namespace std;

class Log {
 private:
  Log();
  virtual ~Log();

  static string getTimeString();

 public:
  // Priority constant for the println method; use Log::v.
  static const int VERBOSE;

  //  Priority constant for the println method; use Log::d.
  static const int DEBUG;

  // Priority constant for the println method; use Log::i.
  static const int INFO;

  //  Priority constant for the println method; use Log::w.
  static const int WARN;

  // Priority constant for the println method; use Log::e.
  static const int ERROR;

  // Setting Log::logLevel to Log::OFF, disables all Log messages.
  static const int OFF;

  // Defaults to Log::VERBOSE.
  static int logLevel;

  static void v(const char* tag, const char* format, ...);

  static void d(const char* tag, const char* format, ...);

  static void i(const char* tag, const char* format, ...);

  static void w(const char* tag, const char* format, ...);

  static void e(const char* tag, const char* format, ...);

  static void println(const char* tag, const char* format, ...);
};

#endif  // LOG_HPP