#ifndef ALIASES_TPP
#define ALIASES_TPP

#include <functional>

namespace aliases {

template <typename T>
using Listener = std::function<void(T)>;

}  // namespace aliases

#endif  // ALIASES_TPP