#ifndef STREAM_CONTROLLER_TPP
#define STREAM_CONTROLLER_TPP

#include "stream.tpp"

template <typename T>
class StreamController {
 private:
  Stream<T>* stream;

 public:
  StreamController() { stream = new Stream<T>(); }

  virtual ~StreamController() {
    if (nullptr == stream) delete stream;
  }

  void add(T event) { stream->add(event); }

  Stream<T>* getStream() const { return stream; }
};

#endif  // STREAM_CONTROLLER_TPP