#ifndef STREAM_TPP
#define STREAM_TPP

#include <vector>

#include "aliases.tpp"
#include "stream_subscription.tpp"

template <typename T>
class StreamController;

template <typename T>
class Stream {
 private:
  friend class StreamController<T>;

  std::vector<StreamSubscription<T>*> subscribers;
  unsigned int currentSubId;

  // Add an event of type T to all listeners.
  void add(T event) {
    for (const StreamSubscription<T>* sub : subscribers)
      if (!sub->isPaused()) sub->_onData(event);
  }

  Stream() : currentSubId(0) {}

 public:
  virtual ~Stream() {
    for (const StreamSubscription<T>* sub : subscribers)
      if (nullptr != sub) delete sub;

    subscribers.clear();
  }

  StreamSubscription<T>* listen(const aliases::Listener<T> onData) {
    StreamSubscription<T>* sub = new StreamSubscription<T>(this, currentSubId);
    currentSubId++;
    sub->onData(onData);
    subscribers.push_back(sub);
    return sub;
  }

  // Check if this stream has any listeners.
  bool hasListeners() const { return !subscribers.empty(); }
};

#endif  // STREAM_TPP