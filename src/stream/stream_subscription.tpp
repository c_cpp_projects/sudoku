#ifndef STREAM_SUBSCRIPTION_TPP
#define STREAM_SUBSCRIPTION_TPP

#include "aliases.tpp"

template <typename T>
class Stream;

template <typename T>
class StreamSubscription {
 private:
  friend class Stream<T>;

  aliases::Listener<T> _onData;
  const unsigned int _id;
  const Stream<T>* _publisher;
  bool _isPaused;

  StreamSubscription(const Stream<T>* publisher, const unsigned id)
      : _publisher(publisher), _id(id), _onData(nullptr) {}

 public:
  virtual ~StreamSubscription() {
    _publisher = nullptr;
    _onData = nullptr;
  }

  friend bool operator==(const StreamSubscription<T>& l, const StreamSubscription<T>& r) {
    if (l._publisher != r._publisher) return false;
    if (l._id != r._id) return false;
    return true;
  }

  friend bool operator!=(const StreamSubscription<T>& l, const StreamSubscription<T>& r) {
    return !(l == r);
  }

  bool isPaused() const { return _isPaused; }

  void onData(const aliases::Listener<T> onData) { _onData = onData; }

  void pause() { _isPaused = true; }

  void resume() { _isPaused = false; }
};

#endif  // STREAM_SUBSCRIPTION_TPP