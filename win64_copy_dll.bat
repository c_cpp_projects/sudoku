@echo off
setlocal enableextensions enabledelayedexpansion

rem This script copies all .dll files from directory to ouput
set "output=%1"
set "binDir=%2"

rem  Check if output is empty
if "" == "%output%" (
    goto EOF
)

rem Copy all .dll file contained in binDir
:LOOP_START
    if not "" == "%binDir%" (
        for /f %%a in ('dir /b /s "%binDir%\*.dll"') do (
            rem split the path to file %%a into its components, like the name and extension
            for %%i in ("%%a") do (
                rem %%~ni is the file name, SDL2 for example
                rem %%~xi is the file extension, .dll for example
        
                rem Copy %%a if it does not exist
                if not exist "%output%\%%~ni%%~xi" (
                   echo Copying: %%a to %output%
                   xcopy /Q %%a "%output%"  
                )
            )
        )
        shift
        set "binDir=%2"
        goto :LOOP_START
    )

:EOF
endlocal